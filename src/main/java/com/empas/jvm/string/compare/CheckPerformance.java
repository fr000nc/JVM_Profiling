package com.empas.jvm.string.compare;

import java.util.Collection;
import java.util.stream.Collectors;

public class CheckPerformance {

    public static void main(String[] args) throws Exception {
        Collection<String> strings = StringGenerator.generate();
        Collection<ComparerBad.ComparableString> badStrings =
                strings.stream().map(ComparerBad.ComparableString::new).collect(Collectors.toList());
        Collection<ComparerGood.ComparableString> goodStrings =
                strings.stream().map(ComparerGood.ComparableString::new).collect(Collectors.toList());

        System.out.println("Press any key...");
        System.in.read();

        System.out.println("Starting test for bad comparer");
        long start = System.currentTimeMillis();
        for (ComparerBad.ComparableString s1 : badStrings) {
            for (ComparerBad.ComparableString s2 : badStrings) {
                s1.compareTo(s2);
                s2.compareTo(s1);
                s1.equals(s2);
                s2.equals(s2);
            }
        }
        long stop = System.currentTimeMillis();
        System.out.println("All operations for bad comparer finished in " + String.valueOf(stop - start) + " ms");

        System.out.println("Starting test for good comparer");
        start = System.currentTimeMillis();
        for (ComparerGood.ComparableString s1 : goodStrings) {
            for (ComparerGood.ComparableString s2 : goodStrings) {
                s1.compareTo(s2);
                s2.compareTo(s1);
                s1.equals(s2);
                s2.equals(s2);
            }
        }
        stop = System.currentTimeMillis();
        System.out.println("All operations for good comparer finished in " + String.valueOf(stop - start) + " ms");
    }
}
