package com.empas.jvm.string.compare;

import java.util.Objects;

public class ComparerGood {

    public static void main(String[] args) {
        ComparableString one = new ComparableString("one");
        ComparableString two = new ComparableString("one");

        System.out.println(one.compareTo(two));
        System.out.println(two.compareTo(one));
    }

    static class ComparableString implements Comparable<ComparableString> {

        public final String string;

        public ComparableString(String s) {
            string = s;
        }


        @Override
        public int compareTo(ComparableString o) {
            if(this.equals(o)){
                return 0;
            }
            int result = 0;
            boolean amILonger = this.string.length() >= o.string.length();
            String longer = this.string.length() >= o.string.length() ? this.string : o.string;
            String shorter = this.string.length() >= o.string.length() ? o.string : this.string;
            for(int i = 0; i < shorter.length(); i++) {
                result += this.string.charAt(i) - o.string.charAt(i);
            }
            for(int i = shorter.length(); i < longer.length(); i++) {
                result += (amILonger ? 1 : -1) * longer.charAt(i);
            }
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ComparableString that = (ComparableString) o;
            return Objects.equals(string, that.string);
        }

        @Override
        public int hashCode() {
            return Objects.hash(string);
        }
    }
}
