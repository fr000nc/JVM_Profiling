package com.empas.jvm.phonenumbers;

public class PhoneNumberMetadata {
    private String localNumber;
    private Country country;

    @Override
    public String toString() {
        return "PhoneNumberMetadata{" +
                "localNumber='" + localNumber + '\'' +
                ", country=" + country +
                '}';
    }

    public PhoneNumberMetadata(String localNumber, Country country) {

        this.localNumber = localNumber;
        this.country = country;
    }

    public String getLocalNumber() {
        return localNumber;
    }

    public void setLocalNumber(String localNumber) {
        this.localNumber = localNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhoneNumberMetadata that = (PhoneNumberMetadata) o;

        if (localNumber != null ? !localNumber.equals(that.localNumber) : that.localNumber != null) return false;
        return country != null ? country.equals(that.country) : that.country == null;
    }

    @Override
    public int hashCode() {
        int result = localNumber != null ? localNumber.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        return result;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public static class Country {
        String prefix;
        String code;

        public Country(String prefix, String code) {

            this.prefix = prefix;
            this.code = code;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Country country = (Country) o;

            if (prefix != null ? !prefix.equals(country.prefix) : country.prefix != null) return false;
            return code != null ? code.equals(country.code) : country.code == null;
        }

        @Override
        public int hashCode() {
            int result = prefix != null ? prefix.hashCode() : 0;
            result = 31 * result + (code != null ? code.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Country{" +
                    "prefix='" + prefix + '\'' +
                    ", code='" + code + '\'' +
                    '}';
        }

        public String getPrefix() {

            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }
    }
}
