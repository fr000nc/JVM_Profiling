package com.empas.jvm.phonenumbers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class StringGenerator {

    public static Collection<String> generate() {
        Random random = new Random();
        Collection<String> col = new ArrayList<>();
        for(int i = 0; i < 100000; i++) {
            int length = random.nextInt(100) + 3;
            byte [] bytes = new byte[length];
            random.nextBytes(bytes);
            if(i%2==0) {
                bytes[2] = 'A';
            }
            ((ArrayList<String>) col).add(new String(bytes));
        }
        return col;
    }

    public static void main(String[] args) {
        System.out.println(generate());
    }
}
