package com.empas.jvm.phonenumbers;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class PhoneNumberParser {

    public PhoneNumberMetadata parseNumber(String number) {
        PhoneNumberUtil utils = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phoneNumber = utils.parse(number, Locale.getDefault().getCountry());
            String countryIso = utils.getRegionCodeForCountryCode(phoneNumber.getCountryCode());
            return new PhoneNumberMetadata(String.valueOf(phoneNumber.getNationalNumber()),
                    new PhoneNumberMetadata.Country(String.valueOf(phoneNumber.getCountryCode()), countryIso));

        } catch(NumberParseException e) {
            throw new RuntimeException(e);
        }
    }

    public List<PhoneNumberMetadata> parseNumberCollection(List<String> numbers) {
        return numbers.stream().map(this::parseNumber).collect(Collectors.toList());
    }

    public boolean containsLocalNumber(List<String> numbers, String localNumber) {
        List<PhoneNumberMetadata> data = parseNumberCollection(numbers);
        List<String> localNumbers = data.stream().map(PhoneNumberMetadata::getLocalNumber).collect(Collectors.toList());
        return localNumbers.contains(localNumber);
    }

}
