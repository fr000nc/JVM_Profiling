package com.empas.jvm.profiling.test;


import com.empas.jvm.phonenumbers.PhoneNumberMetadata;
import com.empas.jvm.phonenumbers.PhoneNumberParser;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class PhoneNumberParserTest {

    private static PhoneNumberParser parser;
    private static PhoneNumberMetadata data500100100;
    private static PhoneNumberMetadata data66600099922;
    private static PhoneNumberMetadata.Country poland;
    private static PhoneNumberMetadata.Country uk;
    private static List<String> numbers;
    private static List<Integer> countryCodes;


    private static void generateRandomNumbers() {
        numbers = new ArrayList<>();
        countryCodes = new ArrayList<>();
        countryCodes.add(44);
        countryCodes.add(48);
        countryCodes.add(1);
        countryCodes.add(7);
        countryCodes.add(347);

        Random random = new Random(System.currentTimeMillis());
        for(int i = 0; i < 100000; i++) {
            StringBuilder builder = new StringBuilder();
            if(i%2==0) {
                builder.append("00");
            } else {
                builder.append("+");
            }
            builder.append(countryCodes.get(random.nextInt(countryCodes.size())));
            builder.append(random.nextInt(999999999));
            numbers.add(builder.toString());
        }
        numbers.add(random.nextInt(numbers.size()), "+48500100100");

    }

    @BeforeClass
    public static void init() {
        parser = new PhoneNumberParser();
        poland = new PhoneNumberMetadata.Country("48", "PL");
        uk = new PhoneNumberMetadata.Country("44", "GB");
        data500100100 = new PhoneNumberMetadata("500100100", poland);
        data66600099922 = new PhoneNumberMetadata("66600099922", uk);
        generateRandomNumbers();
    }

    @Test
    public void parseNumberTest() {
        PhoneNumberMetadata dataPl = parser.parseNumber("+48500100100");
        assertEquals(dataPl,data500100100);
        PhoneNumberMetadata dataUk = parser.parseNumber("004466600099922");
        assertEquals(dataUk,data66600099922);
    }

    @Test
    public void parseNumberCollectionTest() {
        List<PhoneNumberMetadata> data = parser.parseNumberCollection(numbers);
        assertNotNull(data);
    }

    @Test
    public void containsLocalNumberTest() {
        assertTrue(parser.containsLocalNumber(numbers, data500100100.getLocalNumber()));
        assertFalse(parser.containsLocalNumber(numbers, data66600099922.getLocalNumber()));
    }
}
